#include <stdint.h>
#include <stdbool.h>

#include <gio/gio.h>
#include <glib.h>

#ifdef GSTREAMER_ENABLED
#include <gst/gst.h>
#endif

#include "Mumble.pb-c.h"

#include "packet_emitters.h"

void set_size_field(unsigned long size, uint8_t *buffer)
{
    buffer[0] = size >> 24;
    buffer[1] = (size >> 16) & 0xFF;
    buffer[2] = (size >> 8) & 0xFF;
    buffer[3] = size & 0xFF;
}

int send_version_packet(uint16_t major, uint8_t minor, uint8_t patch,
        char *release, char* os, char* os_version, GOutputStream *ostream)
{ 
    uint8_t *buffer;
    unsigned long protobuf_size;
    unsigned long packet_size;
    unsigned long bytes_written;
    MumbleProto__Version version = MUMBLE_PROTO__VERSION__INIT;

    version.has_version = true;
    uint32_t major_shifted = 1 << 16;
    uint32_t minor_shifted = 3 << 8;
    uint32_t patch_shifted = 0;
    version.version = major_shifted | minor_shifted | patch_shifted;
    
    version.release = release;
    version.os = os;
    version.os_version = os_version;
    
    protobuf_size = mumble_proto__version__get_packed_size(&version);
    packet_size = 6 + protobuf_size;

    buffer = malloc(packet_size);
    memset(buffer, 0, packet_size);
    
    buffer[1] = 0;

    set_size_field(protobuf_size, buffer + 2);

    mumble_proto__version__pack(&version, buffer + 6);

    g_output_stream_write_all(ostream, buffer, packet_size, &bytes_written, NULL, NULL); 
    free(buffer);
    return 0;
}

gboolean send_ping_packet(gpointer user_data)
{
    uint8_t *buffer;
    unsigned long protobuf_size;
    unsigned long packet_size;
    unsigned long bytes_written;
    GOutputStream *ostream = (GOutputStream*)user_data; 
    MumbleProto__Ping ping = MUMBLE_PROTO__PING__INIT;

    ping.has_timestamp = true;
    ping.timestamp = g_get_monotonic_time();

    protobuf_size = mumble_proto__ping__get_packed_size(&ping);
    packet_size = 6 + protobuf_size;
    buffer = malloc(packet_size);
    if(!buffer) return false;
    memset(buffer, 0, packet_size);
    buffer[1] = 3;
    set_size_field(protobuf_size, buffer + 2);
    mumble_proto__ping__pack(&ping, buffer + 6);
    g_output_stream_write_all(ostream, buffer, packet_size, &bytes_written, NULL, NULL);
    if(bytes_written < packet_size) return -1;
    free(buffer);   

    return true;
}

int send_authenticate_packet(char *username, char *password, GOutputStream *ostream)
{

    uint8_t *buffer;
    unsigned long protobuf_size;
    unsigned long packet_size;
    unsigned long bytes_written;
    MumbleProto__Authenticate auth = MUMBLE_PROTO__AUTHENTICATE__INIT;

    auth.username = username;
    auth.password = password;
    auth.n_tokens = 0;
    auth.n_celt_versions = 0;
    auth.has_opus = true;
    auth.opus = true;
    protobuf_size = mumble_proto__authenticate__get_packed_size(&auth);
    packet_size = protobuf_size + 6;
    buffer = malloc(packet_size);
    if(!buffer) return -1;
    memset(buffer, 0, packet_size);
    buffer[1] = 2;
    set_size_field(protobuf_size, buffer + 2);
    mumble_proto__authenticate__pack(&auth, buffer + 6);
    g_output_stream_write_all(ostream, buffer, packet_size, &bytes_written, NULL, NULL);
    if(bytes_written < packet_size) return -1;
    free(buffer);
    return 0;
}

#ifdef GSTREAMER_ENABLED
int send_udp_tunnel_packet(struct opus_udp_tunnel_packet packet, GOutputStream *ostream)
{
    gsize bytes_written;
    g_output_stream_write_all(ostream, &packet.tcp_packet_type, 2, &bytes_written, NULL, NULL);
    //printf("bytes_written: %" G_GSIZE_FORMAT "\n", bytes_written);
    g_output_stream_write_all(ostream, &packet.packet_size, 4, &bytes_written, NULL, NULL);
    //printf("bytes_written: %" G_GSIZE_FORMAT "\n", bytes_written);
    g_output_stream_write_all(ostream, &packet.header, 1, &bytes_written, NULL, NULL);
    //printf("bytes_written: %" G_GSIZE_FORMAT "\n", bytes_written);
    g_output_stream_write_all(ostream, packet.sequence_number_varint, packet.sequence_number_varint_size, &bytes_written, NULL, NULL);
    //printf("bytes_written: %" G_GSIZE_FORMAT "\n", bytes_written);
    g_output_stream_write_all(ostream, packet.opus_header_varint, packet.opus_header_varint_size, &bytes_written, NULL, NULL);
    //printf("bytes_written: %" G_GSIZE_FORMAT "\n", bytes_written);
    g_output_stream_write_all(ostream, packet.map.data, packet.map.size, &bytes_written, NULL, NULL);
    //printf("bytes_written: %" G_GSIZE_FORMAT "\n", bytes_written);
    return 0;
}
#endif
