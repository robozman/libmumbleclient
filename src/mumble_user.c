#include "mumble_user.h"


gint compare_MumbleUser_session(gconstpointer a, gconstpointer b)
{
    MumbleUser *user = (MumbleUser*)a;
    uint32_t *session = (uint32_t*)b;

    return !(user->session == *session);
}
