#include <inttypes.h>
#include <stdbool.h>

#include "varint.h"

bool create_varint(uint64_t value, bool is_negative, uint8_t *output_buffer, size_t *output_size)
{
        if (is_negative) {
                output_buffer[0] = 0xF8;
                bool ret = create_varint(value, false, output_buffer + 1, output_size);
                (*output_size)++;
                return ret;
        }
        else if(value < (1 << 7)) {
                output_buffer[0] = (value & 0x7F);
                *output_size = 1;
                return true;
        }
        else if(value < (1 << 14)) {
                output_buffer[0] = (value & 0x3F00) >> 8 | 0x80;
                output_buffer[1] = (value & 0x00FF);
                *output_size = 2;
                return true;
        }
        else if(value < (1 << 21)) {
                output_buffer[0] = (value & 0x1F0000) >> 16 | 0xC0;
                output_buffer[1] = (value & 0x00FF00) >> 8;
                output_buffer[2] = (value & 0x0000FF);
                *output_size = 3;
                return true;
        }
        else if(value < (1 << 28)) {
                output_buffer[0] = (value & 0x0F000000) >> 24 | 0xE0;
                output_buffer[2] = (value & 0x00FF0000) >> 16;
                output_buffer[3] = (value & 0x0000FF00) >> 8;
                output_buffer[4] = (value & 0x000000FF);
                *output_size = 4;
                return true;
        }
        else if(value <= UINT32_MAX) {
                output_buffer[0] = 0xF0;
                output_buffer[1] = (value & 0xFF000000) >> 24;
                output_buffer[2] = (value & 0x00FF0000) >> 16;
                output_buffer[3] = (value & 0x0000FF00) >> 8;
                output_buffer[4] = (value & 0x000000FF);
                *output_size = 5;
                return true;
        }
        else {
                output_buffer[0] = 0xF4;
                output_buffer[1] = (value & 0xFF00000000000000) >> 56;
                output_buffer[2] = (value & 0x00FF000000000000) >> 48;
                output_buffer[3] = (value & 0x0000FF0000000000) >> 40;
                output_buffer[4] = (value & 0x000000FF00000000) >> 32;
                output_buffer[5] = (value & 0x00000000FF000000) >> 24;
                output_buffer[6] = (value & 0x0000000000FF0000) >> 16;
                output_buffer[7] = (value & 0x000000000000FF00) >> 8;
                output_buffer[8] = (value & 0x00000000000000FF);
                *output_size = 9;
                return true;
        }
        return false;
}

int parse_varint(uint8_t *buffer, uint64_t *value, int *size)
{
    if((buffer[0] >> 7) == 0) {
        *value = buffer[0];
        printf("7-bit: %lu ", *value);
        *size = 1;
    }
    else if((buffer[0] >> 6) == 0x2) {
        *value = ((buffer[0] & 0x3f) << 8) | buffer[1];
        printf("14-bit: %lu ", *value);
        *size = 2;
    }
    else if((buffer[0] >> 5) == 0x6) {
        *value = ((buffer[0] & 0x1f) << 16) | (buffer[1] << 8) | buffer[2];
        printf("21-bit: %lu ", *value);
        *size = 3;
    }
    else if((buffer[0] >> 4) == 0xd) {
        *value = ((buffer[0] & 0xf) << 24) | (buffer[1] << 16) | (buffer[2] << 8) | buffer[3];
        printf("28-bit: %lu ", *value);
        *size = 4;
    }
    else if((buffer[0] >> 2) == 0x3c) {
        *value = (buffer[1] << 24) | (buffer[2] << 16) | (buffer[3] << 8) | buffer[4];
        printf("32-bit: %lu ", *value);
        *size = 5;
    }
    else if((buffer[0] >> 2) == 0x3d) {
        memcpy(value, buffer + 1, 8);
        printf("64-bit: %lu ", *value);
        *size = 9;
    }
    else if((buffer[0] >> 2) == 0x3e) {
        printf("negative....");
        uint64_t to_negate;
        int sub_size;
        if(parse_varint(buffer + 1, &to_negate, &sub_size) < 0) return -1;
        *value = -to_negate;
        *size = sub_size + 1;
    }
    else if((buffer[0] >> 2) == 0x3f) {
        printf("literally no idea....");
        return -1;
    } 
    return 0;
}
