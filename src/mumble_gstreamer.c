#define _GNU_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <inttypes.h>

#include <glib.h>

#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <gst/app/gstappsink.h>

#include "mumble.h"
#include "mumble_impl.h"
#include "mumble_user.h"
#include "varint.h"
#include "mumble_gstreamer.h"
#include "packet_emitters.h"

#define BUFFER_SIZE 1

//GstFlowReturn gstreamer_audio_input_callback(GstAppSink *self, struct MumbleClient *client)
GstFlowReturn gstreamer_create_and_send_audio_packet(GstSample *sample, struct MumbleClient *client)
{
        if (client->ostream == NULL) {
                return GST_FLOW_OK;
        }

        static int buffer_index = 0;
        static struct opus_udp_tunnel_packet buffers[BUFFER_SIZE];


        buffers[buffer_index].tcp_packet_type = g_htons(1);

        uint8_t target = 0;
        uint8_t packet_type = 4;

        buffers[buffer_index].header = (packet_type << 5) | target;

        create_varint(client->gstreamer_data.sequence_number, false, buffers[buffer_index].sequence_number_varint, &(buffers[buffer_index].sequence_number_varint_size));

        //GstSample *sample = gst_app_sink_pull_sample(self);
        buffers[buffer_index].sample = sample;
        if (!buffers[buffer_index].sample) {
                fprintf(stderr, "Error pulling sample\n");
                return GST_FLOW_ERROR;
        }
        GstBuffer *buffer = gst_sample_get_buffer(buffers[buffer_index].sample);
        if (!buffer) {
                fprintf(stderr, "Error pulling buffer\n");
                return GST_FLOW_ERROR;
        }
        //GstMemory *memory = gst_buffer_get_memory(buffer, 0);
        buffers[buffer_index].memory = gst_buffer_get_memory(buffer, 0);
        if (!buffers[buffer_index].memory) {
                fprintf(stderr, "Error getting memory\n");
                return GST_FLOW_ERROR;
        }
        //GstMapInfo map;
        gboolean ret = gst_memory_map(buffers[buffer_index].memory, &buffers[buffer_index].map, GST_MAP_READ);
        if (!ret) {
                fprintf(stderr, "Error mapping memory\n");
                return GST_FLOW_ERROR;
        }

        create_varint(buffers[buffer_index].map.size, false, buffers[buffer_index].opus_header_varint, &buffers[buffer_index].opus_header_varint_size);

        buffers[buffer_index].packet_size = g_htonl(1 + buffers[buffer_index].sequence_number_varint_size + buffers[buffer_index].opus_header_varint_size + buffers[buffer_index].map.size);

        buffer_index++;
        //printf("packet recieved\n");
        if (buffer_index == BUFFER_SIZE) {
          //printf("sending packets\n");
            for (int i = 0; i < BUFFER_SIZE; i++) {
                send_udp_tunnel_packet(buffers[i], client->ostream);
            }

            for (int i = 0; i < BUFFER_SIZE; i++) {
                gst_memory_unmap(buffers[i].memory, &buffers[i].map);
                gst_sample_unref(buffers[i].sample);
            }
            buffer_index = 0;
        }

        //gst_memory_unmap(buffers[buffer_index].memory, &buffers[buffer_index].map);
        //gst_sample_unref(buffers[buffer_index].sample);
        client->gstreamer_data.sequence_number++;
        return GST_FLOW_OK;
}

void gstreamer_audio_output_callback(uint8_t *opus, const size_t opus_header, const struct MumbleUser_Internal *user, struct MumbleClient_gst_data *data)
{

    GstBuffer *buffer = gst_buffer_new_allocate(NULL, opus_header & 0x1fff, NULL);
    if (!buffer) {
            fprintf(stderr, "Could not allocated GstBuffer\n");
            return;
    }
    gsize bytes_written = gst_buffer_fill(buffer, 0, opus, opus_header & 0x1fff);
    if (bytes_written < (opus_header & 0x1fff)) {
            fprintf(stderr, "Not all bytes were written\n");
    }
    GstFlowReturn ret = gst_app_src_push_buffer(user->appsrc, buffer);
    //switch (ret) {
    //case GST_FLOW_OK:
    //        printf("GST_FLOW_OK\n");
    //        break;
    //case GST_FLOW_FLUSHING:
    //        fprintf(stderr, "GST_FLOW_FLUSHING\n");
    //        break;
    //case GST_FLOW_EOS:
    //        fprintf(stderr, "GST_FLOW_EOS\n");
    //        break;
    //default:
    //        fprintf(stderr, "Unknown return value\n");
    //        break;
    //}

    // teminator bit
    //if (opus_header & 0x2000) {
    //}
}

void gstreamer_new_user(struct MumbleUser_Internal *user, struct MumbleClient_gst_data *data)
{
    user->appsrc = GST_APP_SRC(gst_element_factory_make("appsrc", NULL));
    GstElement *opusparse = gst_element_factory_make("opusparse", NULL);
    GstElement *opusdec = gst_element_factory_make("opusdec", NULL);

    if(!user->appsrc || !opusparse || !opusdec) {
        printf("Not all elements could be created for user.\n");
        return;
    }

    // configure appsrc to properly resume from stream
    g_object_set(user->appsrc, "do-timestamp", true, NULL);
    g_object_set(user->appsrc, "is-live", true, "format", GST_FORMAT_TIME, NULL);

    gst_bin_add_many(GST_BIN(data->output_pipeline), GST_ELEMENT(user->appsrc), opusparse, opusdec, NULL);

    if (!gst_element_link(GST_ELEMENT(user->appsrc), opusparse)) {
        fprintf(stderr, "user->appsrc and opusparse could not be linked.\n");
        return;
    }
    if (!gst_element_link(opusparse, opusdec)) {
        fprintf(stderr, "opusparse and opusdec could not be linked.\n");
        return;
    }
    if (!gst_element_link(opusdec, data->output_mixer)) {
        fprintf(stderr, "opusdec and data->ouput_mixer could not be linked.\n");
        return;
    }

    gst_element_set_state(GST_ELEMENT(user->appsrc), GST_STATE_PLAYING);
    gst_element_set_state(opusparse, GST_STATE_PLAYING);
    gst_element_set_state(opusdec, GST_STATE_PLAYING);
    //GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(data->output_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "new_user");
}

int init_audio_output(MumbleClient client)
{
    GstPipeline *output_pipeline = GST_PIPELINE(gst_pipeline_new("audio-playback"));
    GstElement *appsrc = gst_element_factory_make("appsrc", "appsrc");
    GstElement *opusparse = gst_element_factory_make("opusparse", NULL);
    GstElement *opusdec = gst_element_factory_make("opusdec", "opusdec");
    GstElement *output_mixer = gst_element_factory_make("audiomixer", "audiomixer");
    GstElement *audio_sink = gst_element_factory_make("autoaudiosink", "audio_sink");

    if (!output_pipeline) {
        fprintf(stderr, "Pipeline could not be created\n");
        return -1;
    }

    if (!appsrc || !opusparse || !opusdec || !output_mixer || !audio_sink) {
        fprintf(stderr, "Not all elements could be created.\n");
        return -1;
    }

    // set name for pulseaudio to see (not working atm)
    g_object_set(audio_sink, "name", "libmumbleclient", NULL);

    // configure cb_data->appsrc to properly resume from stream
    g_object_set(appsrc, "do-timestamp", true, NULL);
    g_object_set(appsrc, "is-live", true, "format", GST_FORMAT_TIME, NULL);

    gst_bin_add_many(GST_BIN(output_pipeline), appsrc, opusparse, opusdec, output_mixer, audio_sink, NULL);

    if (!gst_element_link(appsrc, opusparse)) {
        printf("appsrc and opusparse could not be linked\n");
        return -1;
    }

    if (!gst_element_link(opusparse, opusdec)) {
        printf("opusparse and opusdec could not be linked\n");
        return -1;
    }

    if (!gst_element_link(opusdec, output_mixer)) {
        printf("opusdec and output_mixer could not be linked\n");
        return -1;
    }

    if (!gst_element_link(output_mixer, audio_sink)) {
        printf("output_mixer and audiosink could not be linked\n");
        return -1;
    }

    gst_element_set_state(GST_ELEMENT(output_pipeline), GST_STATE_PLAYING);
    client->gstreamer_data.output_pipeline = output_pipeline;
    client->gstreamer_data.output_mixer = output_mixer;
    return 0;
}

int init_audio_input(MumbleClient client)
{
       GstPipeline *input_pipeline = GST_PIPELINE(gst_pipeline_new("audio-playback"));
       GstElement *autoaudiosrc = gst_element_factory_make("autoaudiosrc", "autoaudiosrc");
       GstElement *opusenc = gst_element_factory_make("opusenc", "opusenc");
       //GstElement *queue2 = gst_element_factory_make("queue2", "queue2");
       GstAppSink *appsink = GST_APP_SINK(gst_element_factory_make("appsink", "appsink"));

       if (!input_pipeline) {
           fprintf(stderr, "Pipeline could not be created\n");
           return -1;
       }

       if (!autoaudiosrc | !opusenc | !appsink) {
           fprintf(stderr, "Not all elements could be created.\n");
           return -1;
       }

       // set name for the OS to see (not working atm)
       g_object_set(autoaudiosrc, "name", "libmumbleclient", NULL);
       g_object_set(opusenc, "bitrate", 40000, NULL);
       g_object_set(opusenc, "frame_size", 10, NULL);
       g_object_set(opusenc, "audio-type", 2048, NULL);
       g_object_set(opusenc, "inband-fec", true, NULL);
       //gst_app_sink_set_emit_signals(appsink, true);
       //g_signal_connect(appsink, "new-sample", G_CALLBACK(gstreamer_audio_input_callback), client);

       //gst_bin_add_many(GST_BIN(input_pipeline), autoaudiosrc, opusenc, queue2, GST_ELEMENT(appsink), NULL);
       gst_bin_add_many(GST_BIN(input_pipeline), autoaudiosrc, opusenc, GST_ELEMENT(appsink), NULL);

       if (!gst_element_link(autoaudiosrc, opusenc)) {
           printf("autoaudiosrc and opusenc could not be linked\n");
           return -1;
       }

       if (!gst_element_link(opusenc, GST_ELEMENT(appsink))) {
         printf("opusenc and queue2 could not be linked\n");
         return -1;
       }
       //
       //       if (!gst_element_link(queue2, GST_ELEMENT(appsink))) {
       //           printf("opusenc and appsink could not be linked\n");
       //           return -1;
       //       }

       gst_element_set_state(GST_ELEMENT(input_pipeline), GST_STATE_PLAYING);
       //GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(input_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "input");

       client->gstreamer_data.input_pipeline = input_pipeline;
       client->gstreamer_data.input_appsink = appsink;
       return 0;
}

int init_gstreamer(MumbleClient client)
{
    if(!gst_init_check(NULL, NULL, NULL)) {
        fprintf(stderr, "Unable to init GStreamer\n");
        return -1;
    }

    if (init_audio_output(client) < 0) {
        fprintf(stderr, "Unable to init audio output\n");
        return -1;
    }

    if (init_audio_input(client) < 0) {
        fprintf(stderr, "Unable to init audio input\n");
        return -1;
    }

    return 0;
}

gboolean audio_check_callback(gpointer user_data)
{
    struct MumbleClient *client = (struct MumbleClient*)user_data;
    GstSample *audio_sample = gst_app_sink_try_pull_sample(client->gstreamer_data.input_appsink, 1);
    if(!audio_sample) {
        return true;
    }
    else {
        gstreamer_create_and_send_audio_packet(audio_sample, client);
        return true;
    }
}
