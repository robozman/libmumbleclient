#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>

#include <gio/gio.h>
#include <glib.h>

#include <opus/opus.h>

#include "Mumble.pb-c.h"

#include "mumble.h"
#include "mumble_impl.h"
#include "packet_handlers.h"
#include "packet_emitters.h"

#ifdef GSTREAMER_ENABLED
#include "mumble_gstreamer.h"
#endif


MumbleClient_Config *MumbleClient_Config_import(MumbleClient_Config config);
bool MumbleClient_Config_validate(MumbleClient_Config config);
bool validate_string(char *string);

MumbleClient MumbleClient_create(MumbleClient_Config config)
{
    if (!MumbleClient_Config_validate(config)) {
        return NULL;
    }

    MumbleClient client = calloc(1, sizeof(struct MumbleClient));
    client->config = MumbleClient_Config_import(config);
    if (!client->config) {
        return NULL;
    }

    return client;
}

void MumbleClient_set_audio_callback(MumbleClient client, MumbleClient_audio_callback audio_callback, void *audio_callback_data)
{
    client->audio_callback = audio_callback;
    client->audio_callback_data = audio_callback_data;
}

void MumbleClient_set_user_callback(MumbleClient client, MumbleClient_user_callback user_callback, void *user_callback_data)
{
    client->user_callback = user_callback;
    client->user_callback_data = user_callback_data;
}

int MumbleClient_enable_gstreamer(MumbleClient client)
{
#ifdef GSTREAMER_ENABLED
        client->gstreamer_enabled = true;
        return 0;
#else
        return -1;
#endif
}

int MumbleClient_connect(MumbleClient client)
{
    if (connect_to_server(client) < 0) {
        fprintf(stderr, "Error connecting to server.\n");
        return -1;
    }

#ifdef GSTREAMER_ENABLED
    if (client->gstreamer_enabled) {
      if(init_gstreamer(client) < 0) {
        fprintf(stderr, "Error initializing GStreamer\n");
        return -1;
      }
    }
#endif

   if (establish_connection(client) < 0) {
       fprintf(stderr, "Error establishing connection.\n");
       return -1;
   }


   if (setup_main_loop(client) < 0) {
       fprintf(stderr, "Error setting up main loop.\n");
       return -1;
   }


   if (start_main_loop(client) < 0) {
       fprintf(stderr, "Error starting main loop.\n");
       return -1;
   }

   return 0;
}

int connect_to_server(MumbleClient client)
{
    GSocketType socket_type = G_SOCKET_TYPE_STREAM;
    GSocketFamily socket_family = G_SOCKET_FAMILY_IPV4;
    client->socket = g_socket_new(socket_family, socket_type, 0, NULL);
    if(!(client->socket)) return -1;
    GSocketConnectable *connectable = g_network_address_new(client->config->server_hostname, client->config->server_port);
    if(!connectable) return -1;
    GSocketAddressEnumerator *enumerator = g_socket_connectable_enumerate(connectable);
    if(!enumerator) return -1;
    GSocketAddress *address = g_socket_address_enumerator_next(enumerator, NULL, NULL);
    if(!address) return -1;
    g_socket_connect(client->socket, address, NULL, NULL);
    g_object_unref(address);
    GSocketAddress *src_address = g_socket_get_local_address(client->socket, NULL);
    if(!src_address) return -1;
    GIOStream *connection = G_IO_STREAM(g_socket_connection_factory_create_connection(
                client->socket));
    GIOStream *tls_conn;
    tls_conn = g_tls_client_connection_new(connection, connectable, NULL);
    g_object_unref(connection);
    connection = G_IO_STREAM(tls_conn);

    if (client->config->verify_server_ssl_certificate) {
        g_tls_client_connection_set_validation_flags(G_TLS_CLIENT_CONNECTION(tls_conn), G_TLS_CERTIFICATE_VALIDATE_ALL);
    }
    else {
        g_tls_client_connection_set_validation_flags(G_TLS_CLIENT_CONNECTION(tls_conn), G_TLS_CERTIFICATE_NOT_ACTIVATED | G_TLS_CERTIFICATE_EXPIRED | G_TLS_CERTIFICATE_REVOKED | G_TLS_CERTIFICATE_INSECURE | G_TLS_CERTIFICATE_GENERIC_ERROR);
    }

    if(!g_tls_connection_handshake(G_TLS_CONNECTION(tls_conn), NULL, NULL)) {
        printf("ERROR: tls handshake failed");
        return -1;
    }
    g_object_unref(connectable);
    client->istream = g_io_stream_get_input_stream(connection);
    client->ostream = g_io_stream_get_output_stream(connection);

    return 0;
}

int establish_connection(MumbleClient client)
{
    uint8_t *protobuf_buffer;
    uint16_t packet_type;
    uint32_t packet_length;

    /* recieve initial version packet from server */
    if(recieve_packet(&packet_type, &packet_length, &protobuf_buffer, client->istream) < 0) {
        printf("ERROR: recieving packet failed\n");
        return -1;
    }
    if(packet_type != 0) {
        printf("ERROR: Packet type %d recieved, was expecting 0\n", packet_type);
        return -1;
    }

    MumbleProto__Version *s_version; /* version packet recieved from server */
    s_version = mumble_proto__version__unpack(NULL, packet_length, protobuf_buffer);
    free(protobuf_buffer);
    /* print version packet */
    if(s_version->has_version == true) {
        uint16_t major = (s_version->version >> 16);
        uint8_t minor = ((s_version->version & 0xFFFF) >> 8);
        uint8_t patch = (s_version->version & 0xFF);
        printf("Server Version: %d.%d.%d\n", major, minor, patch);
    }
    printf("Server Release Name: %s\n", s_version->release);
    printf("Server OS: %s\n", s_version->os);
    printf("Server OS Version: %s\n", s_version->os_version);
    mumble_proto__version__free_unpacked(s_version, NULL);

    if(send_version_packet(1, 3, 0,
                           client->config->client_name,
                           client->config->os,
                           client->config->os_version,
                           client->ostream) < 0) {
        printf("Error sending version packet.\n");
        return -1;
    }

    if(send_authenticate_packet(client->config->username, client->config->password, client->ostream) < 0) {
        printf("Error sending authenticate packet.\n");
        return -1;
    }

    if(recieve_packet(&packet_type, &packet_length, &protobuf_buffer, client->istream) < 0) {
        printf("ERROR: recieving packet failed\n");
        return -1;
    }
    if(packet_type != 15) {
        printf("ERROR: Packet type %d recieved, was expecting 15\n", packet_type);
        return -1;
    }

    MumbleProto__CryptSetup *crypt_setup;
    crypt_setup = mumble_proto__crypt_setup__unpack(NULL, packet_length, protobuf_buffer);
    free(protobuf_buffer);
    mumble_proto__crypt_setup__free_unpacked(crypt_setup, NULL);


    // channelstate, userstate, and serversync packets
    for (;;) {
        if(recieve_packet(&packet_type, &packet_length, &protobuf_buffer, client->istream) < 0) {
            printf("ERROR: recieving packet failed\n");
            return -1;
        }
        switch(packet_type) {
            case SERVER_SYNC:
                //printf("ServerSync packet found\n");
                //printf("Packet Length: %d\n", packet_length);
                handle_server_sync(protobuf_buffer, packet_length);
                free(protobuf_buffer);
                goto sync_complete;
            case CHANNEL_STATE:
                //printf("ChannelState packet found\n");
                //printf("Packet Length: %d\n", packet_length);
                handle_channel_state(protobuf_buffer, packet_length);
                free(protobuf_buffer);
                break;
            case USER_STATE:
                //printf("UserState packet found\n");
                //printf("Packet Length: %d\n", packet_length);
                handle_user_state(protobuf_buffer, packet_length, client);
                free(protobuf_buffer);
                break;
            default:
                break;
        }
    }
sync_complete:
    return 0;
}

int setup_main_loop(MumbleClient client)
{
    GMainLoop *loop = g_main_loop_new(NULL, false);

    GSource *ping_timeout = g_timeout_source_new_seconds(15);
    if(!ping_timeout) return -1;
    GSource *data_available = g_socket_create_source(client->socket, G_IO_IN, NULL);
    if(!data_available) return -1;

    #ifdef GSTREAMER_ENABLED
        GSource *audio_timeout = g_timeout_source_new(1);
        //GSource *audio_cb = g_idle_source_new();
        if(!audio_timeout) return -1;
    #endif

    g_source_set_callback(ping_timeout, send_ping_packet, (gpointer)client->ostream, NULL);

    g_source_set_callback(data_available, (GSourceFunc)data_available_callback, (gpointer)client, NULL);

    #ifdef GSTREAMER_ENABLED
        if (client->gstreamer_enabled) {
            g_source_set_callback(audio_timeout, (GSourceFunc)audio_check_callback, (gpointer)client, NULL);
        }
    #endif

    g_source_attach(ping_timeout, NULL);
    g_source_attach(data_available, NULL);
    g_source_attach(audio_timeout, NULL);

    GThread *main_loop = g_thread_new("connection", start_main_loop, (gpointer)loop);

    //g_main_loop_run(loop);
    //g_main_loop_unref(loop);
    g_thread_join(main_loop);

    return 0;
}

gpointer start_main_loop(gpointer data)
{
    GMainLoop *loop = (GMainLoop*)data;
    g_main_loop_run(loop);
    return NULL;
}


MumblePing_Response MumbleClient_ping_server(char *hostname, uint16_t server_port)
{
    MumblePing_Response response = {.sucessful = false};


    GResolver *resolver = g_resolver_get_default();

    GList *results = g_resolver_lookup_by_name(resolver, hostname, NULL, NULL);
    g_object_unref(resolver);

    if (!results) {
        return response;
    }



    for (GList *l = results; l != NULL; l = l->next) {
        GInetAddress *addr = l->data;

        if ((g_inet_address_get_family(addr) != G_SOCKET_FAMILY_IPV4) &&
            (g_inet_address_get_family(addr) != G_SOCKET_FAMILY_IPV6)) {
            continue;
        }


        GSocketAddress *sockaddr = g_inet_socket_address_new(addr, server_port);

        GSocket *udp_socket = g_socket_new(g_inet_address_get_family(addr), G_SOCKET_TYPE_DATAGRAM,
                                           G_SOCKET_PROTOCOL_UDP, NULL);
        if (!udp_socket) {
            return response;
        }


        gchar ping_packet[12] = {0};

        int32_t request_type = 0;
        int64_t ident = (((int64_t)g_random_int()) << 32) | g_random_int();

        *((int32_t*)(ping_packet)) = g_htonl(request_type);
        *((int64_t*)(ping_packet + 4)) = GINT64_TO_BE(ident);

        gssize sent = g_socket_send_to(udp_socket, G_SOCKET_ADDRESS(sockaddr),
                                       ping_packet, 12, NULL, NULL);
        if (sent != 12) {
            break;
        }

        gchar receive_buffer[24] = {0};

        gssize received = g_socket_receive_from(udp_socket, NULL,
                                                receive_buffer, 24, NULL, NULL);
        g_socket_close(udp_socket, NULL);

        if (received == 24) {
            int64_t received_ident = GINT64_FROM_BE(*((int64_t*)(receive_buffer + 4)));
            if (received_ident != ident) {
                break;
            }
            response.sucessful = true;
            response.version[0] = receive_buffer[0];
            response.version[1] = receive_buffer[1];
            response.version[2] = receive_buffer[2];
            response.version[3] = receive_buffer[3];
            response.connected_users = g_htonl(*((int32_t*)(receive_buffer + 12)));
            response.max_users = g_htonl(*((int32_t*)(receive_buffer + 16)));
            response.allowed_bandwidth = g_htonl(*((int32_t*)(receive_buffer + 20)));
            break;
        }
    }


    g_resolver_free_addresses(results);
    return response;
}

MumbleClient_Config *MumbleClient_Config_import(MumbleClient_Config config)
{
    MumbleClient_Config *new_config = calloc(1, sizeof(MumbleClient_Config));

    // copy all top level data
    *new_config = config;

    // deep copy needed strings, modfifying as config dictates
    new_config->server_hostname = g_strdup(config.server_hostname);
    new_config->server_port = config.server_port;
    new_config->username = g_strdup(config.username);
    new_config->password = g_strdup(config.password);

    // if we are providing client info to the server, fill in fields
    if (config.provide_client_info) {
        if (config.append_libmumbleclient_info_to_client_name) {
            new_config->client_name = g_strconcat(config.client_name, " (libmumbleclient 0.0.1)", NULL);
        }
        else {
            new_config->client_name = g_strdup(config.client_name);
        }
    }
    else {
        new_config->client_name = g_strdup("");
    }

    if (config.provide_os_info) {
        if (config.auto_fill_fields) {
            // ternary: if glib return val is valid use iat, else use empty string
            char *glib_os_name = g_get_os_info(G_OS_INFO_KEY_NAME);
            new_config->os = g_strdup(glib_os_name ? glib_os_name : "");

            char *glib_os_version = g_get_os_info(G_OS_INFO_KEY_VERSION);
            new_config->os_version = g_strdup(glib_os_version ? glib_os_version : "");
        }
        else {
            new_config->os = g_strdup(config.os);
            new_config->os_version = g_strdup(config.os_version);
        }
    }
    else {
        new_config->os = g_strdup("");
        new_config->os_version = g_strdup("");
    }

    new_config->verify_server_ssl_certificate = config.verify_server_ssl_certificate;

    return new_config;
}

bool MumbleClient_Config_validate(MumbleClient_Config config)
{
    if (!validate_string(config.server_hostname)) {
        return false;
    }
    if (!validate_string(config.username)) {
        return false;
    }
    if (config.password == NULL) {
        return false;
    }

    if (config.provide_client_info) {
        if (config.client_name == NULL) {
            return false;
        }
    }

    if (config.provide_os_info && !config.auto_fill_fields) {
        if (config.os == NULL) {
            return false;
        }
        if (config.os_version == NULL) {
            return false;
        }
    }
    return true;
}

bool validate_string(char *string)
{
    return ((string != NULL) && (g_strcmp0(string, "") != 0));
}
