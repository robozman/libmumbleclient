#include "packet_handlers.h"

gboolean data_available_callback(GSocket *socket, GIOCondition condition,
        gpointer user_data)
{
    uint8_t *protobuf_buffer;
    uint16_t packet_type;
    uint32_t packet_length;
    MumbleClient client = (MumbleClient)user_data;


    if(recieve_packet(&packet_type, &packet_length, &protobuf_buffer, client->istream) < 0) {
        printf("ERROR: recieving packet failed\n");
        return false;
    }

    switch(packet_type) {
        case UDP_TUNNEL:
            handle_udp_tunnel(protobuf_buffer, packet_length, client);
            break;
        case CHANNEL_STATE:
            handle_channel_state(protobuf_buffer, packet_length);
            break;
        case USER_REMOVE:
            handle_user_remove(protobuf_buffer, packet_length, client);
            break;
        case USER_STATE:
            handle_user_state(protobuf_buffer, packet_length, client);
            break;
        case TEXT_MESSAGE:
            handle_text_message(protobuf_buffer, packet_length);
            break;
        default:
            break;
    }
    free(protobuf_buffer);

    return true;
}

int recieve_packet(uint16_t *packet_type, uint32_t *protobuf_length,
        uint8_t **protobuf_buffer, GInputStream *istream)
{
    uint8_t *buffer;
    unsigned long bytes_read;

    buffer = malloc(6);
    if(!buffer) return -1;
    memset(buffer, 0, 6);

    g_input_stream_read_all(istream, buffer, 6, &bytes_read, NULL, NULL);
    if(bytes_read < 6) return -1;
    
    *packet_type = (buffer[0] << 8) | buffer[1];
    *protobuf_length = (buffer[2] << 24) | (buffer[3] << 16) | (buffer[4] << 8) | buffer[5];
    free(buffer);
    
    *protobuf_buffer = malloc(*protobuf_length);
    if(!(*protobuf_buffer)) return -1;
    memset(*protobuf_buffer, 0, *protobuf_length);

    g_input_stream_read_all(istream, *protobuf_buffer, *protobuf_length, &bytes_read, NULL, NULL);
    if(bytes_read < *protobuf_length) return -1;

    return 0;
}

int handle_udp_tunnel(uint8_t *buffer, int packet_length, MumbleClient client)
{
    printf("=======================================\n");
    if(buffer == NULL) return -1;
    if(packet_length <= 0) return -1;
    //switch(buffer[0] >> 5)
      //{
      //    case 0:
      //        printf("CELT-A ");
      //        break;
      //    case 1:
      //        printf("Ping ");
      //        break;
      //    case 2:
      //        printf("Speex ");
      //        break;
      //    case 3:
      //        printf("CELT-B ");
      //        break;
      //    case 4:
      //        printf("OPUS ");
      //        break;
      //    default:
      //        break;
      //}

    switch(buffer[0] & 0x1F)
    {
        case 0:
            /* normal voice target */
            break;
        default:
            break;
    }

    buffer += 1;

    uint64_t session_id;
    int session_id_size;
    printf("Session ID: ");
    if(parse_varint(buffer, &session_id, &session_id_size) < 0) return -1;
    //printf("session_id_size: %d\n", session_id_size);
    buffer += session_id_size;

    uint64_t sequence_number;
    int sequence_number_size;
    printf("Sequence Number: ");
    if(parse_varint(buffer, &sequence_number, &sequence_number_size) < 0) return -1;
    //printf("sequence_number_size: %d\n", sequence_number_size);
    buffer += sequence_number_size;


    uint64_t opus_header;
    int opus_header_size;
    printf("Opus Header: ");
    if(parse_varint(buffer, &opus_header, &opus_header_size) < 0) return -1;
    //printf("opus_header_size: %d\n", opus_header_size);
    buffer += opus_header_size;
    if ((opus_header & 0x2000)) {
        printf("Terminator");
    }

    GList *found_user = g_list_find_custom(client->user_list, (gconstpointer)&session_id, compare_MumbleUser_session);
    if (!found_user) {
        printf("User not in list\n");
        return -1;
    }

    struct MumbleUser_Internal *user = (struct MumbleUser_Internal*)found_user->data;
    printf("\nAudio data recieved from user %s with session id %d.\n", user->pub_user.name, user->pub_user.session);

    if (client->audio_callback) {
        client->audio_callback(buffer, opus_header, &user->pub_user, client->audio_callback_data);
    }

#ifdef GSTREAMER_ENABLED
    if (client->gstreamer_enabled) {
            gstreamer_audio_output_callback(buffer, opus_header, user, &client->gstreamer_data);
    }
#endif
    return 0;
}
int handle_channel_state(uint8_t *buffer, int packet_length)
{
    if(buffer == NULL) return -1;
    if(packet_length <= 0) return -1;
    MumbleProto__ChannelState *channel_state; /* ChannelState packet recieved from server */
    channel_state = mumble_proto__channel_state__unpack(NULL, packet_length, buffer);
    if(channel_state == NULL) return -1;
    printf("Channel State: ");
    if(channel_state->has_channel_id) printf("| ID: %d ", channel_state->channel_id);
    if(channel_state->has_parent) printf("| Parent ID: %d ", channel_state->parent);
    printf("| Name: %s ", channel_state->name);
    printf("| Description: %s ", channel_state->description);
    if(channel_state->has_temporary) printf("| Temporary: %s ", channel_state->temporary ? "True" : "False");
    if(channel_state->has_position) printf("| Position: %d ", channel_state->position);
    if(channel_state->has_max_users) printf("| Max Users: %d\n", channel_state->max_users);
    mumble_proto__channel_state__free_unpacked(channel_state, NULL);
    return 0;
}

int handle_user_state(uint8_t *buffer, int packet_length, MumbleClient client)
{
    if(buffer == NULL) return -1;
    if(packet_length <= 0) return -1;
    MumbleProto__UserState *user_state; /* ChannelState packet recieved from server */
    user_state = mumble_proto__user_state__unpack(NULL, packet_length, buffer);
    if(user_state == NULL) return -1;
    printf("User State: ");
    if(user_state->has_session) printf("| Session: %d ", user_state->session);
    if(user_state->has_actor) printf("| Actor: %d ", user_state->actor);
    printf("| Name: %s ", user_state->name);
    if(user_state->has_user_id) printf("| ID: %d ", user_state->user_id);
    if(user_state->has_channel_id) printf("| Channel ID: %d ", user_state->channel_id);
    if(user_state->has_mute) printf("| Mute: %s ", user_state->mute ? "True" : "False");
    if(user_state->has_deaf) printf("| Deaf: %s ", user_state->deaf ? "True" : "False");
    if(user_state->has_suppress) printf("| Suppress: %s ", user_state->suppress ? "True" : "False");
    if(user_state->has_self_mute) printf("| Self Mute: %s ", user_state->self_mute ? "True" : "False");
    if(user_state->has_self_deaf) printf("| Self Deaf: %s ", user_state->self_deaf ? "True" : "False");
    printf("| Comment: %s ", user_state->comment);
    printf("| Hash: %s ", user_state->hash);
    if(user_state->has_priority_speaker) printf("| Priority Speaker: %s ", user_state->priority_speaker ? "True" : "False");
    if(user_state->has_recording) printf("| Recording: %s ", user_state->recording ? "True" : "False");

    GList *user = g_list_find_custom(client->user_list, (gconstpointer)&(user_state->session), compare_MumbleUser_session);
    if(user == NULL) { 
        printf("| User not in list \n");

        struct MumbleUser_Internal *new_user = malloc(sizeof(struct MumbleUser_Internal));
        new_user->pub_user.session = user_state->session;
        new_user->pub_user.name = strdup(user_state->name);
        client->user_list = g_list_append(client->user_list, (gpointer)new_user);
        if (client->user_callback) {
                client->user_callback(&new_user->pub_user, client->user_callback_data, true);
        }
#ifdef GSTREAMER_ENABLED
        gstreamer_new_user(new_user, &client->gstreamer_data);
#endif
    }
    else {
        printf("| User in list \n");
        /* TODO: implement */
    }
    mumble_proto__user_state__free_unpacked(user_state, NULL);
    return 0;
}

int handle_user_remove(uint8_t *buffer, int packet_length, MumbleClient client)
{
    return 0;
}

int handle_server_sync(uint8_t *buffer, int packet_length)
{
    if(buffer == NULL) return -1;
    if(packet_length <= 0) return -1;
    MumbleProto__ServerSync *server_sync; /* ChannelState packet recieved from server */
    server_sync = mumble_proto__server_sync__unpack(NULL, packet_length, buffer);
    if(server_sync == NULL) return -1;
    if(server_sync->has_session) printf("Session: %d\n", server_sync->session);
    if(server_sync->has_max_bandwidth) printf("Max Bandwidth: %d\n", server_sync->max_bandwidth);
    printf("Welcome Text: %s\n", server_sync->welcome_text);
    if(server_sync->has_permissions) printf("Permissions: %ld\n", server_sync->permissions);
    mumble_proto__server_sync__free_unpacked(server_sync, NULL);
    return 0;
}

int handle_text_message(uint8_t *buffer, int packet_length)
{
    if(buffer == NULL) return -1;
    if(packet_length <= 0) return -1;
    MumbleProto__TextMessage *text_message; /* ChannelState packet recieved from server */
    text_message = mumble_proto__text_message__unpack(NULL, packet_length, buffer);
    if(text_message == NULL) return -1;
    if(text_message->has_actor) printf("Actor: %d\n", text_message->actor);
    printf("Message: %s\n", text_message->message);
    mumble_proto__text_message__free_unpacked(text_message, NULL);
    return 0;
}


