#ifndef PACKET_HANDLERS_H
#define PACKET_HANDLERS_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "mumble.h"
#include "mumble_impl.h"
#include "mumble_user.h"
#include "varint.h"

#include "Mumble.pb-c.h"

enum packet_types
{
    VERSION = 0,
    UDP_TUNNEL = 1,
    AUTHENTICATE = 2,
    PING = 3,
    REJECT = 4,
    SERVER_SYNC = 5,
    CHANNEL_REMOVE = 6,
    CHANNEL_STATE = 7,
    USER_REMOVE = 8,
    USER_STATE = 9,
    BAN_LIST = 10,
    TEXT_MESSAGE = 11,
    PERMISSION_DENIED = 12,
    ACL = 13,
    QUERY_USERS = 14,
    CRYPT_SETUP = 15,
    CONTEXT_ACTION_MODIFY = 16,
    CONTEXT_ACTION = 17,
    USER_LIST = 18,
    VOICE_TARGET = 19,
    PERMISSION_QUERY = 20,
    CODEC_VERSION = 21,
    USER_STATS = 22,
    REQUEST_BLOB = 23,
    SERVER_CONFIG = 24,
    SUGGEST_CONFIG = 25
};


gboolean data_available_callback(GSocket *socket, GIOCondition condition,
        gpointer user_data);
int recieve_packet(uint16_t *packet_type, 
        uint32_t *protobuf_length, uint8_t **protobuf_buffer, GInputStream *istream);
int handle_udp_tunnel(uint8_t *buffer, int packet_length, MumbleClient client);
int handle_server_sync(uint8_t *buffer, int packet_length);
int handle_channel_state(uint8_t *buffer, int packet_length);
int handle_user_remove(uint8_t *buffer, int packet_length, MumbleClient client);
int handle_user_state(uint8_t *buffer, int packet_length, MumbleClient client);
int handle_text_message(uint8_t *buffer, int packet_length);

#endif /* PACKET_HANDLERS_H */
