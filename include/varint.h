#ifndef VARINT_H
#define VARINT_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

bool create_varint(uint64_t value, bool is_negative, uint8_t *output_buffer, size_t *output_size);

int parse_varint(uint8_t *buffer, uint64_t *value, int *size);

#endif /* VARINT_H */
