#ifndef MUMBLE_H
#define MUMBLE_H

#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

typedef struct MumbleClient* MumbleClient;

typedef struct
{
    char *server_hostname;
    uint16_t server_port;
    char *username;
    char *password;

    bool provide_client_info;
    bool append_libmumbleclient_info_to_client_name;
    char *client_name;

    bool provide_os_info;
    bool auto_fill_fields;
    char *os;
    char *os_version;

    bool *verify_server_ssl_certificate;

} MumbleClient_Config;

typedef struct
{
    uint32_t session;
    char *name;
    uint32_t user_id;
    uint32_t channel_id;
    bool mute;
    bool deaf;
    bool supress;
    bool self_mute;
    bool self_deaf;
    char *comment;
    char *hash;
    bool priority_speaker;
    bool recording;
} MumbleUser;

typedef struct
{
    bool sucessful;
    uint8_t version[4];
    int connected_users;
    int max_users;
    int allowed_bandwidth;
} MumblePing_Response;

typedef void (*MumbleClient_audio_callback)(uint8_t*, const size_t, const MumbleUser *user, void*);
typedef void (*MumbleClient_user_callback)(const MumbleUser *user, void*, bool);

MumbleClient MumbleClient_create(MumbleClient_Config config);

void MumbleClient_set_audio_callback(MumbleClient client, MumbleClient_audio_callback audio_callback, void *audio_callback_data);
void MumbleClient_set_user_callback(MumbleClient client, MumbleClient_user_callback user_callback, void *user_callback_data);
int MumbleClient_enable_gstreamer(MumbleClient client);
int MumbleClient_connect(MumbleClient client);
MumblePing_Response MumbleClient_ping_server(char *hostname, uint16_t server_port);

MumbleClient_Config MumbleClient_get_config(MumbleClient client);

#endif /* MUMBLE_H */
