#ifndef MUMBLE_GSTREAMER_H
#define MUMBLE_GSTREAMER_H

#include <gio/gio.h>
#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#include "mumble.h"
#include "mumble_user.h"

struct MumbleClient_gst_data {
    GList *user_list;
    GstPipeline *output_pipeline;
    GstElement *output_mixer;
    GstPipeline *input_pipeline;
    GstAppSink *input_appsink;
    uint64_t sequence_number;
};

int init_gstreamer(MumbleClient client);
void gstreamer_audio_output_callback(uint8_t *opus, const size_t opus_header, const struct MumbleUser_Internal *user, struct MumbleClient_gst_data *data);
void gstreamer_new_user(struct MumbleUser_Internal *user, struct MumbleClient_gst_data *data);
gboolean audio_check_callback(gpointer user_data);
#endif /* MUMBLE_GSTREAMER_H */
