#ifndef PACKET_EMITTERS_H
#define PACKET_EMITTERS_H

#include <stdint.h>

#include <gio/gio.h>

#include "Mumble.pb-c.h"

#ifdef GSTREAMER_ENABLED
#include <gst/gst.h>
struct opus_udp_tunnel_packet {
  uint16_t tcp_packet_type;
  uint32_t packet_size;
  uint8_t header;
  uint8_t sequence_number_varint[10];
  size_t sequence_number_varint_size;
  uint8_t opus_header_varint[10];
  size_t opus_header_varint_size;
  GstSample *sample;
  GstMemory *memory;
  GstMapInfo map;
};
int send_udp_tunnel_packet(struct opus_udp_tunnel_packet, GOutputStream *ostream);
#endif

int send_version_packet(uint16_t major, uint8_t minor, uint8_t patch,
                        char *release, char* os, char* os_version, GOutputStream *ostream);
gboolean send_ping_packet(gpointer user_data);
int send_authenticate_packet(char *username, char *password, GOutputStream *ostream);

#endif /* PACKET_EMITTERS_H */
