#ifndef MUMBLE_IMPL_H
#define MUMBLE_IMPL_H

#include <gio/gio.h>
#include <glib.h>

#include "mumble.h"

#ifdef GSTREAMER_ENABLED
#include "mumble_gstreamer.h"
#endif

struct MumbleClient
{
    MumbleClient_Config *config;

    GList *user_list;
    GList *channel_list;

    GSocket *socket;
    GInputStream *istream;
    GOutputStream *ostream;

    MumbleClient_audio_callback audio_callback;
    void *audio_callback_data;

    MumbleClient_user_callback user_callback;
    void *user_callback_data;

    GThread *main_loop;

#ifdef GSTREAMER_ENABLED
    gboolean gstreamer_enabled;
    struct MumbleClient_gst_data gstreamer_data;
#endif
};


int connect_to_server(MumbleClient client);
int establish_connection(MumbleClient client);
int setup_main_loop(MumbleClient client);
gpointer start_main_loop(gpointer data);
gboolean data_available_callback(GSocket *socket, GIOCondition condition, gpointer user_data);

#endif /* MUMBLE_IMPL_H */
