#ifndef MUMBLE_USER_H
#define MUMBLE_USER_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <glib.h>

#ifdef GSTREAMER_ENABLED
#include <gst/app/gstappsrc.h>
#endif

#include "mumble.h"

struct MumbleUser_Internal {
    MumbleUser pub_user;
#ifdef GSTREAMER_ENABLED
    GstAppSrc *appsrc;
#endif
};

gint compare_MumbleUser_session(gconstpointer a, gconstpointer b);

#endif /* MUMBLE_USER_H */
